"use strict";

app.activatedView = kendo.observable({

    serial: "",
    continueMessage: "",
    retryMessage: "",
    canRetry: false,
    canContinue: false,

    back() {
        const confirm = app.activatedView;

        confirm.set("confirming", false);

        app.install.go("ACTIVATE", true);
    },

    continue() {
        app.install.go("GEOMETRY");
    },

    retry() {
        if (app.install.type === 2) {
            //643
            app.install.go("PRIME-643", true);
        } else if (app.install.type === 3) {
            //608 HO
            app.install.go("PRIME-RFHO", true);
        } else if (app.install.type === 4) {
            //608 LP
            app.install.go("PRIME-RFLP", true);
        } else {
            //Impossible unless debug mode
            app.install.go("SELECT", true);
        }
    }

});