"use strict";

app.selectCustomerView = kendo.observable({
    hasTankList: false,
    hasError: false,

    error: "",
    filter: "",
    tank: "",
    tanks: [],

  emptyFunc() {

    },

    disableBackButton() {
        document.addEventListener("backbutton", app.selectCustomerView.emptyFunc, false);

    },
    enableBackButton() {
        document.removeEventListener("backbutton", app.selectCustomerView.emptyFunc);

    },
   
    select() {
        app.debug("SELECT-CUSTOMER--SELECT", this.tank);

        if (!this.tank) {
            return;
        }

        const blocks = this.tank.split(":");

        const customer = blocks[0];
        const container = blocks[1];

        app.install.setCustomerTank(customer, container);

        if (!app.config.only750) {
            app.install.go("SELECT");
        } else {
            app.install.go("HOTSPOT");    
        }
        
    },

    setTanks(tanks, error) {
        const customerSelect = this;

        customerSelect.set("tanks", tanks || []);
        customerSelect.set("tank", "");

        if (customerSelect.tanks.length === 0) {

            customerSelect.set("error",
                error ||
                `No tanks could be found for
                the account id <strong>${this.filter}</strong>.<br><br>
                Please check the account id and try this again.`
            );

            customerSelect.set("hasTankList", false);
            customerSelect.set("hasError", true);

        } else {

            const selectCustomer = $("#select-customer");

            selectCustomer.empty();

            tanks.forEach(function(tank, index) {
                const value = tank.customerId + ':' + tank.id;

                if (index === 0) {
                    customerSelect.set("tank", value);
                }

                $("<option>",
                {
                    value: value,
                    text: `${tank.customer} - ${tank.address} | Tank ${tank.name} - ${tank.product.name}`
                }).appendTo(selectCustomer);
            });

            app.bootstrapSelect("#select-customer");

            customerSelect.set("hasError", false);
            customerSelect.set("hasTankList", true);

        }
    },

    search() {
        app.debug("SELECT-CUSTOMER--SEARCH", this.filter);

        if (!this.filter) {
            app.debug("No filter provided, returning");
            return;
        }

        app.loader(true);

        app.comms
            .tanks(this.filter)
            .then(tanks => {
                this.setTanks(tanks);

                app.loader(false);
            })
            .catch(error => {
                this.setTanks(null, error);

                app.loader(false);
            });
    }


});

