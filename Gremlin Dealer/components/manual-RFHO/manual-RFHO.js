"use strict";

app.manualRFHOView = kendo.observable({

    error: false,
    serial: "",

    back() {
        app.install.go("CONFIGURE-RFHO", true);
    },

    continue() {
        const configuration = app.manualRFHOView;

        app.debug("MANUAL-RFHO", "Proceeding with serial > ", configuration.serial);

        if (!configuration.serial) {
            configuration.set("error", true);
            return;
        }

        configuration.set("error", false);

        app.install.setMonitor(configuration.serial);
        app.install.go("CONFIRM");

    }

});
