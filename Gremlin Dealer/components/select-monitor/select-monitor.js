"use strict";

app.selectMonitorView = kendo.observable({
    back() {
        app.install.go("CUSTOMER", true);
    },

    onTypeChange(event) {
        const selection = event.data[0];

        app.debug("MONITOR-TYPE-CHANGE", selection.title, selection.type);

        app.install.setMonitorType(selection.type);
    },

    init() {
        const selecter = app.selectMonitorView;

        let scroller = $("#monitors").data("kendoMobileScrollView");

        if (!scroller) {

            app.debug("SELECT-MONITOR-INIT", "Bootstrapping scroller");

            scroller = $("#monitors").kendoMobileScrollView({
                contentHeight: "300px",
                change: selecter.onTypeChange,
                template: kendo.template($("#monitor-template").html())
            }).data("kendoMobileScrollView");


            scroller.setDataSource(monitors);
            scroller.refresh();

        } else {
            app.debug("SELECT-MONITOR-INIT", "Scroller already bootstrapped");
        }
    },

    continue() {
        app.debug("SELECT-MONITOR", "Continue", app.install.type);

        if (app.install.type === 1) {

            if (!window.WifiWizard || app.config.disableAutoDiscovery) {
                app.install.setMonitorFirstInstall(true); 
                app.install.go("PRIME");

                app.debug("SELECT-MONITOR", "No auto discovery, performing monitor first install");
            } else {
                app.install.setMonitorFirstInstall(false);
                app.install.go("HOTSPOT");

                app.debug("SELECT-MONITOR", "Found auto discovery, moving on");
            }

        } else if (app.install.type === 2) {
            app.install.go("CONFIGURE-643");
        } else if (app.install.type === 3) {
            app.install.go("CONFIGURE-RFHO");
        } else if (app.install.type === 4) {
            app.install.go("CONFIGURE-RFLP");
        } else {
            app.debug("SELECT-MONITOR", "No next step provided for monitor type", app.install.type);
        }
    }

});

const monitors = [
    {
        "title": "750 WiFi GREMLIN®",
        "image": "shared/images/monitors/750.png",
        "type": 1
    },
    {
        "title": "643 Cellular Monitor",
        "image": "shared/images/monitors/643.jpg",
        "type": 2
    },
    {
        "title": "RFHO Monitor",
        "image": "shared/images/monitors/RFHO.png",
        "type": 3
    },
    {
        "title": "RFLP Monitor",
        "image": "shared/images/monitors/RFLP.png",
        "type": 4
    }
];