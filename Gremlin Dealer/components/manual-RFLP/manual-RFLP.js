"use strict";

app.manualRFLPView = kendo.observable({

    error: false,
    serial: "",

    back() {
        app.install.go("CONFIGURE-RFLP", true);
    },

    continue() {
        const configuration = app.manualRFLPView;

        app.debug("MANUAL-RFLP", "Proceeding with serial > ", configuration.serial);

        if (!configuration.serial) {
            configuration.set("error", true);
            return;
        }

        configuration.set("error", false);

        app.install.setMonitor(configuration.serial);
        app.install.go("CONFIRM");

    }

});
