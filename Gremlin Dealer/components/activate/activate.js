"use strict";

app.activateView = kendo.observable({
    confirming: false,
    attempts: 0,

    back() {
        const activate = app.activateView;

        activate.set("confirming", false);
        activate.set("attempts", 0);

        if (app.install.type === 2) {
            //643
            app.install.go("PRIME-643", true);
        } else if (app.install.type === 3) {
            //608 HO
            app.install.go("PRIME-RFHO", true);
        } else if (app.install.type === 4) {
            //608 LP
            app.install.go("PRIME-RFLP", true);
        } else {
            //Impossible unless debug mode
            app.install.go("SELECT", true);
        }
    },

    init(e) {
        if (!app.state.auth) {
            e.preventDefault();

            app.debug("APP", "Not authorized for this view, redirecting back to log in", e);
            app.mobileApp.navigate(app.config.routes.auth);

            return false;
        }

        const activate = app.activateView;

        activate.set("confirming", true);
        activate.set("attempts", 0);

        return true;
    }

});


const beutifyMessage = (() => {

    const important = [
        "NOT",
        "GOOD",
        "RELOCATING"
    ];
    const seperator = "<br><br>";

    const importantWords = new RegExp(
        `(${important.join("|").toUpperCase()})`,
        "ig"
    );


    return text => {
        if (!text) {
            return "";
        }

        //Emphasize certain words
        text = text.replace(importantWords, `<span class="message--important">$&</span>`);

        //Break on first period
        text = text.replace(".", "."+seperator);
        if (text.endsWith(seperator)) {
            text = text.substring(0, text.length - seperator.length);
        }

        return text;
    };
})();



(function (activate, activatedView) {

    const DEFAULT_ERROR_MESSAGE = "is not broadcasting. Please try adjusting the placement of your monitor and try setting up the monitor again";

    const ACTIVATE_TIME = 30 * 1000; // 30 seconds 
    const ACTIVATE_INTERVAL = 5 * 1000; // Attempt every 5 seconds
    const ACTIVATE_TIMEOUT = 1 * 4500; // Fail attempt after a 4.5 second wait

    const ACTIVATE_ATTEMPTS = (ACTIVATE_TIME / ACTIVATE_INTERVAL); // 6 attempts will be made based on above parameters

    const increment = packet => {

        packet = packet || {
            "continueMessage": "",
            "canContinue": false,
            "retryMessage": DEFAULT_ERROR_MESSAGE,
            "canRetry": true
        };

        const attempts = activate.attempts + 1;

        app.debug(
            "ACTIVATE-PACKET",
            JSON.stringify(packet),
            "attempts > ",
            attempts,
            "canContinue >",
            packet.canContinue
        );

        activate.set("working", false);
        activate.set("attempts", attempts);

        let canContinue = packet.canContinue;

        if (attempts >= ACTIVATE_ATTEMPTS) {
            canContinue = true;
        }

        if (app.install.currentRoute !== "ACTIVATE" || !canContinue) {
            return;   
        }

        packet.continueMessage = beutifyMessage(packet.continueMessage);
        packet.retryMessage = beutifyMessage(packet.retryMessage);

        activate.set("confirming", false);

        activatedView.set("serial", app.install.monitor);
        activatedView.set("continueMessage", packet.continueMessage);
        activatedView.set("retryMessage", packet.retryMessage);
        activatedView.set("canContinue", packet.canContinue);
        activatedView.set("canRetry", packet.canRetry);

        app.install.go("ACTIVATED");

    };

    const validate = () => {
        if (!activate.confirming
            || activate.working 
            || app.install.currentRoute !== "ACTIVATE") {
            return;
        }

        const options = {};

        options.method = "POST";
        options.url = app.comms.getEndpoint(app.config.validateEndpoint);
        options.timeout = ACTIVATE_TIMEOUT;

        options.data = {};
        options.data.serial = app.install.monitor;
        options.data.monitorType = app.install.type;

        activate.set("working", true);

        $.ajax(options) //TODO: Move to comms
            .done(result => {
                app.debug("ACTIVATE-RESULT", JSON.stringify(result));

                if (result && result.error) {
                    increment();

                } else {
                    increment(result);

                }
            })
            .fail(error => {
                app.debug("ACTIVATE-ERROR", JSON.stringify(error));

                increment();
            });
    };

    setInterval(validate, ACTIVATE_INTERVAL);

})(app.activateView, app.activatedView);
