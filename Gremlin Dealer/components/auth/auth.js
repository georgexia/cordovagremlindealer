'use strict';

app.authView = kendo.observable({

    username: '',
    usernameError: '',
    password: '',
    passwordError: '',
    dealer: '',
    dealerError: '',
    error: '',
    isNotProd: false,
    remember: false,
    toggled: false,

    loadSavedAUTH: function () {
        app.debug("AUTH-INIT");

        let auth = app.authView;

        if (localStorage.getItem("remember")) {
            app.debug("AUTH-INIT", "Loading user information from local storage");

            auth.set("remember", true);
            auth.set("username", localStorage.getItem("username") || "");
            auth.set("dealer", localStorage.getItem("dealer") || "");
            auth.set("password", localStorage.getItem("password") || "");

            const toggled = localStorage.getItem("toggled");

            auth.set("toggled", !!toggled);
        }
    },

    toggle() {
        const prime = app.authView;

        prime.set("toggled", !prime.toggled);
    },

    auth: function () {
        let auth = this;

        $(window).scrollTop(0);

        auth.set("error", "");
        auth.set("usernameError", "");
        auth.set("passwordError", "");
        auth.set("dealerError", "");

        let dealer = auth.dealer;

        if (!auth.toggled) {
            dealer = '';
        }

        let error = false;
        let request = app.utils.getAuthRequest(auth.username, auth.password, dealer, auth.remember);

        if (!request.username) {
            auth.set("usernameError", "Please enter your username.");
            error = true;
        }

        if (!request.password) {
            auth.set("passwordError", "Please enter your password.");
            error = true;
        }

        if (error) {
            return;
        }

        app.debug("AUTH", auth.username, auth.password, auth.dealer, auth.remember);
        app.debug("AUTH-REQUEST", request);

        if (request.remember) {
            localStorage.setItem("remember", true);
            localStorage.setItem("username", auth.username);
            localStorage.setItem("dealer", auth.dealer);
            localStorage.setItem("password", auth.password);

            localStorage.setItem("toggled", auth.toggled ? "true" : "");
        } else {
            localStorage.setItem("remember", "");
            localStorage.setItem("username", "");
            localStorage.setItem("dealer", "");
            localStorage.setItem("password", "");
            localStorage.setItem("toggled", "");
        }

        app.loader(true);
        app.comms.reset();

        auth.set("isNotProd", request.target !== "prod");

        app.comms
            .setABOSEndpoint(request.target, request.dealer)
            .then(function () {
                return app.comms.auth(request.username, request.password);
            })
            .then(function () {
                app.debug("AUTH-SUCESS");

                app.loader(false);

                if (!request.remember) {
                    auth.set("username", "");
                    auth.set("dealer", "");
                    auth.set("password", "");
                    auth.set("toggled", false);
                }

                app.state.login();
            })
            .catch(function (error) {
                app.debug("AUTH-ERROR", error);

                app.comms.reset();
                app.loader(false);

                auth.set("error", error);
            });
    }

});

app.authView.set('title', 'Log In');