"use strict";

app.manual643View = kendo.observable({

    error: false,
    serial: "",

    back() {
        app.install.go("CONFIGURE-643", true);
    },

    continue() {
        const configuration = app.manual643View;

        app.debug("MANUAL-643", "Proceeding with serial > ", configuration.serial);

        if (!configuration.serial) {
            configuration.set("error", true);
            return;
        }

        configuration.set("error", false);

        app.install.setMonitor(configuration.serial);
        app.install.go("CONFIRM");
    }

});
