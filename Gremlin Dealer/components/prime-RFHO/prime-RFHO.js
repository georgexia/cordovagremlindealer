"use strict";

app.primeRFHOView = kendo.observable({

    timeout: false,

    back() {
        app.install.go("CONFIRM", true);
    },

    continue() {
        app.install.go("ACTIVATE");
    }

});
