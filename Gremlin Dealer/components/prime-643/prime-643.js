"use strict";

app.prime643View = kendo.observable({

    counting: true,
    countdown: 15,

    back() {
        app.install.go("CONFIRM", true);
    },

    continue() {
        app.install.go("ACTIVATE");
    },

    init() {
        app.prime643View.set("countdown", 15);
        app.prime643View.set("counting", true);
    }

});

setInterval(() => {
    const prime = app.prime643View;

    if (prime.counting && app.install.currentRoute === "PRIME-643") {
        const time = prime.countdown - 1;

        prime.set("countdown", time);

        app.debug("PRIME-643", "Countdown before aloud forward", time);

        if (time === 1) {
            prime.set("counting", false);
            prime.set("countdown", 15);
        }
    }
}, 1000)