"use strict";


app.selectHotspotView = kendo.observable({
    scanner: false,
    init: false,
    loading: true,
    ready: false,
    error: false,
    password: "",
    ssid: "",

    toggleScanner() {
        app.selectHotspotView.set("scanner", !app.selectHotspotView.scanner);
    },

    back() {
        if (app.monitorFirst) {
            app.install.go("MONITOR", true);
            return;
        }

        if (!app.config.only750) {
            app.install.go("SELECT", true);
        } else {
            app.install.go("CUSTOMER", true);
        }
    },

    select() {
        const hotspots = app.selectHotspotView;

        app.debug("SELECT-HOTSPOT", hotspots.ssid, hotspots.password);

        if (!hotspots.ssid) {
            app.debug("SELECT-HOTSPOT", "Missing SSID");
            return;
        }

        app.install.setHotspot(hotspots.ssid, hotspots.password);

        if (app.install.monitorFirst) {
            app.loader(true);

            setupMonitor(
                    app.comms.monitor.url,
                    app.comms.monitor.port,
                    app.install.ssid,
                    app.install.password
                )
                .then(() => disconnect())
                .then(() => throttledPromise(() => app.comms.ping(), 7, 500, 500, true))
                .then(() => {
                    app.loader(false);

                    app.install.go("PRIME-750");
                })
                .catch(() => {
                    app.loader(false);

                    app.message("GREMLIN® Monitor",
                        `<p>We could not find the monitor.<p><p>Please make sure you are connected to the GREMLIN® HotSpot.<p>`
                    );

                    app.install.go("MONITOR", true);
                });
            return;
        }

        if (!hotspots.scanner || app.config.disableAutoDiscovery) {
            app.install.setHotspot(hotspots.ssid, hotspots.password);

            app.install.go("PRIME");
            return;
        }

        app.loader(true);

        connectToWIFINetwork(hotspots.ssid, hotspots.password).then(() => {
            app.install.setHotspot(hotspots.ssid, hotspots.password);
            app.install.go("PRIME");

            app.loader(false);
        }).catch((error) => {
            app.debug("SELECT-HOTSPOT", 'Error when attemping connection to WiFi hotspot', error);

            app.message("Local WiFi Setup",
                `<p>We could not reach hotspot <strong>${hotspots.ssid}</strong>.<p><p>Please try again.<p>`);
            app.loader(false);
        });
    },

    reloadHotspots() {
        const hotspots = app.selectHotspotView;

        app.debug("SELECT-HOTSPOT", "Reloading hotspots");

        hotspots.set("error", false);

        if (true) {
        // if (app.install.monitorFirst) {
            app.debug("SELECT-HOTSPOT", "Hotspots requested first");
            
            hotspots.set("scanner", true);
            hotspots.set("loading", false);
            hotspots.set("init", true);

            const networks = app.monitorHotspotView.ssids;

            if (!networks || networks.length === 0) {
                onWIFIScanError(hotspots, "No networks to connect to!");
                return;
            }

            displayWIFINetworks(hotspots, networks, true);

            return;
        }


        if (hotspots.loading && hotspots.init) {
            app.debug("SELECT-HOTSPOT", "Hotspot list already loading");
            return;
        }

        if (!window.WifiWizard) {
            hotspots.set("scanner", false);
            hotspots.set("loading", false);

            app.debug("SELECT-HOTSPOT", "Device does not support hotspot wizard");
            return;
        }

        hotspots.set("scanner", true);
        hotspots.set("loading", true);
        hotspots.set("init", true);

        scanWIFINetworks(hotspots);
    }

});

const assureNetworkConfiguration = (ssid, password) => {
    return new Promise((resolve, reject) => {
        const configuration = window.WifiWizard.formatWifiConfig(
            ssid,
            password,
            password ? "WPA" : undefined
        );

        app.debug(
            "SELECT-HOTSPOT",
            `Appending network to configurations`,
            configuration
        );

        window.WifiWizard.addNetwork(configuration, resolve, reject);

//        //get current WIFI
//        window.WifiWizard.getCurrentSSID(ssid => {
//                app.debug("SELECT-HOTSPOT", `Current SSID is ${ssid}`);
//                if (ssid )
//                
//            },
//            reject
//        );

    });
};

const connectToWIFINetwork = (ssid, password) => {
    return assureNetworkConfiguration(ssid, password).then(() => {

        return new Promise((resolve, reject) => {
            app.debug("SELECT-HOTSPOT", "Connecting to SSID");

            window.WifiWizard.connectNetwork(ssid, resolve, reject);
        });
    });
};

const scanWIFINetworks = (hotspots) => {
    app.debug("SELECT-HOTSPOT", "Starting WiFi scan");

    window.WifiWizard.startScan(
        () => onWIFIScanStarted(hotspots),
        error => onWIFIScanError(hotspots, error)
    );
};

const onWIFIScanStarted = (hotspots) => {
    app.debug("SELECT-HOTSPOT", "Scan start OK, attaching callback for results");

    window.WifiWizard.getScanResults(
        networks => networks.length === 0 ? onWIFIScanError(hotspots, "No networks to connect to!") :  displayWIFINetworks(hotspots, networks),
        error => onWIFIScanError(hotspots, error)
    );
};

const displayWIFINetworks = (hotspots, networks, manual) => {

    app.debug("SELECT-HOTSPOT", "Network scan complete");
    app.debug(networks);

    const container = $("#select-network-container");
    const selectNetwork = $("#select-network");

    selectNetwork.empty();

    if (!manual) {
        networks.sort((networkA, networkB) => networkB.level - networkA.level);

        container.removeClass("manual");
    } else {
        if (!container.hasClass("manual")) {
            container.addClass("manual");
        }
    }

    networks.forEach(function(network, index) {
        const value = network.SSID;

        if (!value.trim()) {
            return;
        }

        let ssid = network.SSID;

        ssid = app.utils.encode(ssid);

        let extraInfo = "";

        if (!manual) {
            let level = Math.min(Math.max(2 * (network.level + 100), 0), 100);
            let mac = network.BSSID;

            level = app.utils.encode(level);
            mac = app.utils.encode(mac);

            extraInfo = `
                <div class="item-content">Strength: ${level}%<div>
                <div class="item-footer">${mac}</div>
            `;
        }

       
        if (index === 0) {
            hotspots.set("ssid", value);
        }

        $("<option>",
            {
                value: value,
                text: `
                    <span class="item-header">${ssid}</span>${extraInfo}
                `
            }
        ).appendTo(selectNetwork);

    });

    app.bootstrapSelect("#select-network", false);

    hotspots.set("networks", networks);
    hotspots.set("loading", false);
};


const onWIFIScanError = (hotspots, error) => {
    app.debug("SELECT-HOTSPOT", "Could not scann for networks");
    app.debug(error);

    hotspots.set("error", true);
    hotspots.set("loading", false);
    hotspots.set("scanner", false);
};