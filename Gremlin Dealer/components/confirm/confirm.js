"use strict";

app.confirmView = kendo.observable({
    serial: "",

    back() {
        if (app.install.type === 2) {
            //643
            if (app.install.manual) {
                app.install.go("MANUAL-643", true);
            } else {
                app.install.go("CONFIGURE-643", true);
            }
        } else if (app.install.type === 3) {
            if (app.install.manual) {
                app.install.go("MANUAL-RFHO", true);
            } else {
                app.install.go("CONFIGURE-RFHO", true);
            }
        } else if (app.install.type === 4) {
            if (app.install.manual) {
                app.install.go("MANUAL-RFLP", true);
            } else {
                app.install.go("CONFIGURE-RFLP", true);
            }
        } else {
            app.install.go("SELECT", true);
        }
    },

    init(e) {
        if (!app.state.auth) {
            e.preventDefault();

            app.debug("APP", "Not authorized for this view, redirecting back to log in", e);
            app.mobileApp.navigate(app.config.routes.auth);

            return false;
        }

        const confirm = app.confirmView;

        confirm.set("serial", app.install.monitor);

        return true;
    },

    continue() {
        if (app.install.type === 2) {

            app.prime643View.init();

            //643
            app.install.go("PRIME-643");
        } else if (app.install.type === 3) {

            //608 HO
            app.install.go("PRIME-RFHO");

        } else if (app.install.type === 4) {

            //608 LP
            app.install.go("PRIME-RFLP");

        }

    }

});