'use strict';

(function (app) {

    app.state = kendo.observable({

        auth: false,
        branding: false,
        logo: "",

        goToInstallVideo: function () {
            app.debug("APP-INSTALL-VIDEO");

            app.utils.openLink(app.config.installVideoURL);
        },

        logout: function () {
            app.debug("APP-LOGOUT");

            app.mobileApp.navigate(app.config.routes.auth, "");

            app.state.set("auth", false);
            app.state.set("branding", false);
            app.state.set("logo", "");

            app.comms.reset();
           
            app.install.clear();


            $("#navigation-container li").removeClass("active");
            $("#login-link").addClass("active");
        },
    
        login: function () {
            app.debug("APP-LOGIN");

            app.install.set("currentRoute", app.config.override.route || "CUSTOMER");

            app.mobileApp.navigate(app.config.routes.install);
            app.state.set("auth", true);

            $("#navigation-container li").removeClass("active");
            $("#post-login-link").addClass("active");
        }
    });

    app.authorized = function (e) {
        app.loader(false);

        if (app.state.auth) {
            return true;
        }

        e.preventDefault();

        app.debug("APP", "Not authorized for this view, redirecting back to log in", e);
        app.mobileApp.navigate(app.config.routes.auth);

        return false;
    }

    app.message = function(title, contents, options, onclose) {
        const dialog = $('#dialog');

        options = options ||
        [
            {
                text: "OK",
                primary: true
            }
        ];

        dialog.kendoDialog({
            width: "300px",
            title: title,
            closable: false,
            modal: true,
            content: contents,
            actions: options,
            close: onclose
        });

        dialog.data("kendoDialog").open();
    };

    app.loader = function (state) {
        kendo.ui.progress($("#kendoUiMobileApp"), state);
    };

    app.debug = function () {
        if (!app.config.debug) {
            return;
        }

        const args = Array.from(arguments).map(arg => {
            if (Array.isArray(arg)) {
                return JSON.stringify(arg, null, 2);
            }
            return arg;
        });

        console.log.apply(
            console,
            args
        );
    };
 
}(window.app = {}));