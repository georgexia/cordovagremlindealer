'use strict';

(function (config) {
    
     config.debug = true;
     config.override = {
        // customer: "123456",
        // container: "34937",
        //ssid: "Funky Coffee",
        //password: "9876543210",
        // type: 2, //1=750, 2=643, 3=RFHO
        // route: "PRIME",
        //ignoreMonitorConnect: true,
        //monitor: "861075026510412"
     };

    config.firmwareEndpoint = 'http://monitors.paygo.net:345';
    config.validateEndpoint = "tms/validate";
    config.target = "prod";
    config.disableAutoDiscovery = true;

    config.installVideoURL = "https://youtu.be/9SJI6BJTDBY";

    config.endpoints = {};
    config.endpoints.prod = "https://support.paygo.net";
    config.endpoints.qa = "https://qa.paygo.net";
    config.endpoints.dev = "http://8.39.160.52:88"; 
    config.endpoints.feature = "http://8.39.160.52:87";
    config.endpoints.local = "http://localhost:3000";

    config.routes = {};
    config.routes.root = 'components';
    config.routes.auth = `${config.routes.root}/auth/auth.html`;
    config.routes.install = `${config.routes.root}/install/install.html`;
    config.routes.geometry = `${config.routes.root}/geometry/geometry.html`;

    config.routes.selectCustomer = `${config.routes.root}/select-customer/select-customer.html`;
    config.routes.selectMonitor = `${config.routes.root}/select-monitor/select-monitor.html`;
    config.routes.selectHotspot = `${config.routes.root}/select-hotspot/select-hotspot.html`;

    config.routes.configure643 = `${config.routes.root}/configure-643/configure-643.html`;
    config.routes.configureRFHO = `${config.routes.root}/configure-RFHO/configure-RFHO.html`;
    config.routes.configureRFLP = `${config.routes.root}/configure-RFLP/configure-RFLP.html`;

    config.routes.primeMonitor = `${config.routes.root}/prime-monitor/prime-monitor.html`;
    config.routes.prime643 = `${config.routes.root}/prime-643/prime-643.html`;
    config.routes.primeRFHO = `${config.routes.root}/prime-RFHO/prime-RFHO.html`;
    config.routes.primeRFLP = `${config.routes.root}/prime-RFLP/prime-RFLP.html`;
    config.routes.prime750 = `${config.routes.root}/prime-750/prime-750.html`;

    config.routes.manual643 = `${config.routes.root}/manual-643/manual-643.html`;
    config.routes.manualRFHO = `${config.routes.root}/manual-RFHO/manual-RFHO.html`;
    config.routes.manualRFLP = `${config.routes.root}/manual-RFLP/manual-RFLP.html`;

    config.routes.activate = `${config.routes.root}/activate/activate.html`;
    config.routes.activated = `${config.routes.root}/activated/activated.html`;

    config.routes.confirm = `${config.routes.root}/confirm/confirm.html`;

    config.routes.monitorHotspot = `${config.routes.root}/monitor-hotspot/monitor-hotspot.html`;

}(app.config = {}));

app.config.override = app.config.override || {};