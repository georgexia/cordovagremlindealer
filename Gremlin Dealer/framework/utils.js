'use strict';

(function (utils, config) {

    utils.top = function() {
        $(".km-content:visible")
            .data("kendoMobileScroller")
            .animatedScrollTo(0, 0);
    };

    utils.keepActiveState = function (item) {
        var currentItem = item;
        $('#navigation-container li.active').removeClass('active');
        currentItem.addClass('active');
    };

    utils.openLink = function (url) {
        window.open(url, '_system');
        if (window.event) {
            window.event.preventDefault && window.event.preventDefault();
            window.event.returnValue = false;
        }
    }

    utils.encode = function(input) {
        const el = document.createElement("div");
        el.innerText = el.textContent = input;
        input = el.innerHTML;
        return input;
    }

    utils.getAuthRequest = function (username, password, dealer, remember) {

        username = username.trim();
        password = password.trim();
        dealer = (dealer || "").trim();

        var assertDealer = dealer.toLowerCase();
        var assertUsername = username.toLowerCase();

        var target = config.target;

        // POINT ABOSTEST > QA
        if (assertDealer.startsWith("abostest")) {
            target = "qa";

            dealer = dealer.substring("abostest".length);
        } else {
            for (var environment in config.endpoints) {

                // QA_USER @ PROD
                //    USER @ QA
                if (assertUsername.startsWith(`${environment.toLowerCase()}_`)) {
                    username = username.substring(environment.length + 1);
                    target = environment;
                    break;
                }

            }
        }

        return {
            username: username,
            password: password,
            dealer: dealer,
            remember: remember,
            target: target
        }
    };

    utils.shapeRequiresHeight = function(shapeId) {
        switch (shapeId) {
        case (1):
        case (2):
        case (3):
        case (4):
            return true;
        default:
            return false;
        }
    };

    utils.shapeRequiresLength = function(shapeId) {
        switch (shapeId) {
        case (1):
        case (2):
        case (3):
        case (4):
        case (5):
            return true;
        default:
            return false;
        }
    };

    utils.shapeRequiresWidth = function(shapeId) {
        switch (shapeId) {
        case (1):
        case (2):
        case (3):
        case (4):
            return true;
        default:
            return false;
        }
    };

    utils.shapeRequiresDiameter = function(shapeId) {
        switch (shapeId) {
        case (5):
        case (6):
        case (7):
            return true;
        default:
            return false;
        }
    };

    utils.shapeWidthIsHeight = function(shapeId) {
        switch (shapeId) {
        case (6):
        case (7):
            return true;
        default:
            return false;
        }
    };

})(window.app.utils = {}, window.app.config);