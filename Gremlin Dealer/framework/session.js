'use strict';

(function (session) {

    session.state = {};

    session.destroy = function () {
        session.state = {};
    };

})(window.app.session = {});