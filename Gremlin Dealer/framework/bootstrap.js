(function (app) {

    const applyOverrides = (overrides) => {
        app.debug("BOOTSTRAP", "Applying overrides", overrides);


        // WIFI
        if (overrides.ssid) {
            app.selectHotspotView.set("ssid", overrides.ssid);
        }
        if (overrides.password) {
            app.selectHotspotView.set("password", overrides.password);
        }
        if (overrides.ssid || overrides.password) {
            app.install.setHotspot(
                overrides.ssid || "",
                overrides.password || ""
            );
        }

        // CUSTOMER
        if (overrides.customer) {
            app.selectCustomerView.set("filter", overrides.customer);

            app.install.set("customer", overrides.customer);
        }

        // MONITOR
        if (overrides.monitor) {
            app.install.setMonitor(overrides.monitor);

            app.manual608View.set("serial", overrides.monitor);
            app.manual643View.set("serial", overrides.monitor);
        }

        // CONTAINER
        if (overrides.container) {
            app.install.set("container", overrides.container);
        }

        // MONITOR TYPE
        if (overrides.type) {
            app.install.setMonitorType(overrides.type);
        }
    };

    var init = function () {

        $(function () {

            if (app.config.debug && app.config.weinre) {
                app.debug("BOOTSTRAP", "Attatching weinre script", app.config.weinre);

                $("body").append($("<script>", {
                    src: `http://${app.config.weinre.target}/target/target-script-min.js#${app.config.weinre.title}`
                }));
            }

            app.mobileApp = new kendo.mobile.Application(document.body, {
                skin: "nova",
                initial: "components/auth/auth.html"
            });

            kendo.bind($("#kendoUiMobileApp"), app.state);

            const selectFormatting = function (text) {
                var newText = text;

                const findreps = [
                    { find: /^([^\-]+) \- /g, rep: '<span class="item-header">$1</span>' },
                    { find: /([^\|><]+) \| /g, rep: '<div class="item-content">$1</div>' },
                    { find: /([^\|><\(\)]+) (\()/g, rep: '<div class="item-content">$1</div>$2' },
                    { find: /([^\|><\(\)]+)$/g, rep: '<div class="item-content">$1</div>' },
                    { find: /(\([^\|><]+\))$/g, rep: '<div class="item-footer">$1</div>' }
                ];

                for (var i in findreps) {
                    newText = newText.replace(findreps[i].find, findreps[i].rep);
                }
                return newText;
            }

            app.bootstrapSelect = function (selector, noFomatting) {
                const options = {
                    width: '90%',
                    height: '40px'
                };

                if (!noFomatting) {
                    options.format = selectFormatting;
                }

                $(selector || '[data-role="select"]').selectmenu(options);
            };

            $("#init-non-auth").addClass("active");

            if (app.config.override) {
                applyOverrides(app.config.override);
            }

        });
    
        //Disable the back button completely
       /* $(document).ready(function () {
            document.addEventListener("backbutton", backKeyDown, true);

            app.debug("Hardware Back Button Disabled");

        });

        function backKeyDown() {

        };
        */
    };

    if (window.cordova) {
// ReSharper disable once Html.EventNotResolved
        document.addEventListener("deviceready", function () {

            if (navigator && navigator.splashscreen) {
                navigator.splashscreen.hide();
            }
            if (window.navigator.msPointerEnabled) {
                $("#navigation-container").on("MSPointerDown", "a", function (event) {
                    app.utils.keepActiveState($(this));
                });
            } else {
                $("#navigation-container").on("touchstart", "a", function (event) {
                    app.utils.keepActiveState($(this).closest("li"));
                });
            }

            init();
        }, false);
    } else {
        init();
    }

}(window.app));