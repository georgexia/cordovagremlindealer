"use strict";

app.confirmView = kendo.observable({
    serial: "",

    back() {
        if (app.install.type === 2) {
            //643
            if (app.install.manual) {
                app.install.go("MANUAL-643", true);
            } else {
                app.install.go("CONFIGURE-643", true);
            }
        } else if (app.install.type === 3) {
            if (app.install.manual) {
                app.install.go("MANUAL-608", true);
            } else {
                app.install.go("CONFIGURE-608", true);
            }
        } else {
            app.install.go("SELECT", true);
        }
    },

    init(e) {
        if (!app.state.auth) {
            e.preventDefault();

            app.debug("APP", "Not authorized for this view, redirecting back to log in", e);
            app.mobileApp.navigate(app.config.routes.auth);

            return false;
        }

        const confirm = app.confirmView;

        confirm.set("serial", app.install.monitor);

        return true;
    },

    continue() {
        if (app.install.type === 2) {

            app.prime643View.init();

            //643
            app.install.go("PRIME-643");
        } else {

            //608
            app.install.go("PRIME-608");

        }

    }

});