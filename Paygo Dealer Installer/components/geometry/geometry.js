"use strict";
app.geometryView = kendo.observable({
    prior: false,

    type: 0,
    height: 0,
    width: 0,
    length: 0,
    diameter: 0,
    offset: 0,
    outlet: 0,
    capacity: 100,
    antenna: 1,
    location: 1,
    dual: false,
    previous: false,

    firstSelection:false,
    isPropane: false,
    automatic: false,

    shapeRequiresHeight: false,
    shapeRequiresLength: false,
    shapeRequiresWidth: false,
    shapeRequiresDiameter: false,
    shapeWidthIsHeight: false,

    back() {
        if (app.install.type === 1) {
            app.install.go("PRIME-750", true);
        } else if (app.install.type === 2) {
            app.install.go("ACTIVATED", true);
        } else if (app.install.type === 3 || app.install.type === 4) {
            app.install.go("CONFIRM", true);
        } else {
            app.debug("GEOMETRY", "No back step available for monitor type > ", app.install.type);
        }
    },

    setType(type) {
        const geometry = app.geometryView;

        geometry.set("type", type);

        geometry.set("shapeRequiresHeight", app.utils.shapeRequiresHeight(type));
        geometry.set("shapeRequiresLength", app.utils.shapeRequiresLength(type));
        geometry.set("shapeRequiresWidth", app.utils.shapeRequiresWidth(type));
        geometry.set("shapeRequiresDiameter", app.utils.shapeRequiresDiameter(type));
        geometry.set("shapeWidthIsHeight", app.utils.shapeWidthIsHeight(type));
    },

    onTypeChange(event) {
        const geometry = app.geometryView;

        const selection = event.data[0];

        app.debug("GEOMETRY-TYPE-CHANGE", selection);

        geometry.set("usesWidth", selection.shape.width);
        geometry.set("automatic", !!selection.preset);

        geometry.setType(selection.shape.type);

        if (selection.preset) {
            if (selection.preset.diameter) {
                geometry.set("diameter", selection.preset.diameter);
            }

            geometry.set("height", selection.preset.height);
            geometry.set("width", selection.preset.width);
            geometry.set("length", selection.preset.length);
            geometry.set("offset", selection.preset.offset);
            geometry.set("outlet", selection.preset.outlet);
            geometry.set("capacity", selection.preset.safe);
        } else if (!geometry.firstSelection) {

            geometry.set("height", 0);
            geometry.set("diameter", 0);
            geometry.set("width", 0);
            geometry.set("length", 0);
            geometry.set("offset", 0);
            geometry.set("outlet", 0);
            geometry.set("capacity", 100);
        }

        geometry.set("firstSelection", false);

        app.debug("GEOMETRY-POST-CHANGE", geometry);
    },

    preinit(e) {
        if (!app.state.auth) {
            e.preventDefault();

            app.debug("APP", "Not authorized for this view, redirecting back to log in", e);
            app.mobileApp.navigate(app.config.routes.auth);

            return false;
        }

        const geometry = app.geometryView;

        geometry.set("isPropane", app.install.type !== 1);

        return true;
    },

    init() {

        app.loader(true);

        const geometry = app.geometryView;

        app.debug("GEOMETRY-INIT", app.install.monitor);

        geometry.set("prior", false);
        geometry.set("firstSelection", false);

        const selector = `#geometry-${!geometry.isPropane ? 'non' : ''}propane`;
        const selections = geometry.isPropane ? selectionsPropane : selectionsNonPropane;

        let scroller = $(selector).data("kendoMobileScrollView");

        if (!scroller) {

            app.debug("GEOMETRY-INIT", "Bootstrapping scroller");

            scroller = $(selector).kendoMobileScrollView({
                contentHeight: "200px",
                change: geometry.onTypeChange,
                template: kendo.template($("#geometry-template").html())
            }).data("kendoMobileScrollView");

            scroller.setDataSource(selections);
            scroller.refresh();

        } else {
            app.debug("GEOMETRY-INIT", "Scroller already bootstrapped");
        }

        scroller.refresh();

        app.comms
            .container(app.install.monitor)
            .then(container => {
                app.debug("GEOMETRY-GET-CONTAINER", container);

                let type = container.shape.id;
                let width = (app.utils.shapeRequiresWidth(container.shape.id) ? container.dimensions.width : 0) || 0;
                let height = (app.utils.shapeRequiresHeight(container.shape.id) ? container.dimensions.height : 0) || 0;
                let diameter = (app.utils.shapeRequiresDiameter(container.shape.id) ? container.dimensions.width : 0) || 0;
                let length = container.dimensions.length || 0;
                let outlet = container.outletHeight || 0;
                let offset = container.sensorOffset || 0;
                let capacity = container.safeCapacity || 100;
           
                geometry.set("height", height);
                geometry.set("width", width);
                geometry.set("diameter", diameter);
                geometry.set("length", length);
                geometry.set("offset", offset);
                geometry.set("outlet", outlet);
                geometry.set("capacity",capacity);
                geometry.set("location", container.location.id);
                geometry.set("antenna", container.monitorAntennaType.id);

                if (app.utils.shapeRequiresDiameter(type)) {
                    width = diameter;
                    height = length;
                }
                if (app.utils.shapeWidthIsHeight(type)) {
                    height = width;
                }

                app.debug("GEOMETRY", type, height, width, length, offset, outlet, capacity);

                geometry.setType(type);

                let selection = selections.find(entry => {
                    return entry.shape.type === type &&
                        entry.preset &&
                        entry.preset.height === height &&
                        entry.preset.width === width &&
                        entry.preset.length === length &&
                        entry.preset.offset === offset &&
                        entry.preset.outlet === outlet &&
                        entry.preset.safe === capacity;
                });

                if (!selection) {
                    selection = selections.find(entry => {
                        return entry.shape.type === type && !entry.preset;
                    });
                }

                if (selection) {
                    geometry.set("firstSelection", true);

                    scroller.scrollTo(selections.indexOf(selection));

                    geometry.set("usesWidth", selection.shape.width);

                    if (selection.preset) {
                        geometry.set("automatic", true);

                        geometry.set("height", selection.preset.height);
                        geometry.set("width", selection.preset.width);
                        geometry.set("length", selection.preset.length);
                        geometry.set("offset", selection.preset.offset);
                        geometry.set("outlet", selection.preset.outlet);
                        geometry.set("capacity", selection.preset.safe);
                    } else {
                        geometry.set("automatic", false);
                    }

                    
                } 

                app.loader(false);
            })
            .catch(error => {
                app.debug("GEOMETRY-GET-CONTAINER", error);
                app.loader(false);

                geometry.set("outlet", 0);
                geometry.set("capacity", 100);

                scroller.scrollTo(0);
            });

    },

    continue() {
        const geometry = app.geometryView;

        app.debug("GEOMETRY", "Saving tank geometry", geometry);

        app.loader(true);

        let length = geometry.length;
        if (geometry.dual && !geometry.isPropane && geometry.automatic) {
            app.debug("GEOMETRY", "Doubling length");

            length *= 2;
        }

        let type = geometry.type;
        let width  = geometry.width;
        let height = geometry.height;
        let diameter = geometry.diameter;

        if (app.utils.shapeRequiresDiameter(type)) {
            width = diameter;
            height = length;
        }
        if (app.utils.shapeWidthIsHeight(type)) {
            height = width;
        }

        let location = geometry.location;
        let antenna = geometry.antenna;

        if (app.install.monitor !== 1) {
            location = 2; //Outdoor enclosed
            antenna = 2;  //External
        }

        app.comms.map(
            app.install.container,
            app.install.monitor
        )
        .then(() => app.comms
        .geometry(
            app.install.container,
            location,
            antenna,
            type,
            height,
            width,
            length,
            geometry.offset,
            geometry.outlet,
            geometry.capacity,
            app.install.type
        ))
        .then(() => {
            app.loader(false);

            app.message("Install Completed",
                `<p>Monitor ID ${app.install.monitor} has been installed for Customer Acct. ID ${app.install.customer}<p>`
            );

            app.install.clear();

            app.install.go("CUSTOMER", true);
        })
        .catch(error => {
            app.loader(false);

            app.debug("GEOMETRY", "Could not save", error);

            app.message("Tank Setup",
                `<p>We could not save the tank's geometry<p><p>Please make sure this information is filled out correctly.<p>`
            );
        });
    },

    antennaOptions: [
        {
            "title": "Internal",
            "type": 1
        },
        {
            "title": "External",
            "type": 2
        }
    ],

    locationOptions: [
        {
            "title": "Indoors",
            "type": 1
        },
        {
            "title": "Outdoor - Unenclosed",
            "type": 2
        },
        {
            "title": "Outdoor - Enclosed",
            "type": 3
        }
    ]
});

const shapes = {
    "r": {
        "title": "Rectangular/Roth",
        "image": "shared/images/geometry/r.png",
        "width": true,
        "type": 4
    },
    "ov": {
        "title": "Oval Vertical Other",
        "image": "shared/images/geometry/ov.png",
        "width": true,
        "type": 1
    },
    "oh": {
        "title": "Oval Horizontal Other",
        "image": "shared/images/geometry/oh.png",
        "width": true,
        "type": 2
    },
    "ou": {
        "title": "Oval Upright",
        "image": "shared/images/geometry/ov.png",
        "width": true,
        "type": 3
    },
    "cv": {
        "title": "Cylindrical Vertical",
        "image": "shared/images/geometry/cv.png",
        "width": false,
        "type": 5
    },
    "ch": {
        "title": "Cylindrical Horizontal",
        "image": "shared/images/geometry/ch.png",
        "width": false,
        "type": 6
    },
    "chd": {
        "title": "Cylindrical Horizontal Dished",
        "image": "shared/images/geometry/chh.png",
        "width": false,
        "type": 7
    }
};

const selectionsNonPropane = [
    {
        "title": `Oval Vertical 275`,
        "preset": {
            "height": 44,
            "width": 27,
            "length": 60,
            "offset": 0,
            "outlet": 0,
            "safe": 100
        },
        shape: shapes["ov"]
    },
    {
        "title": `Oval Vertical 330`,
        "preset": {
            "height": 44,
            "width": 27,
            "length": 72,
            "offset": 0,
            "outlet": 0,
            "safe": 100
        },
        shape: shapes["ov"]
    },
    {
        "title": `Oval Horizontal 275`,
        "preset": {
            "height": 27,
            "width": 44,
            "length": 60,
            "offset": 0,
            "outlet": 0,
            "safe": 100
        },
        shape: shapes["oh"]
    },
    {
        "title": `Oval Horizontal 330`,
        "preset": {
            "height": 27,
            "width": 44,
            "length": 72,
            "offset": 0,
            "outlet": 0,
            "safe": 100
        },
        shape: shapes["oh"]
    }
].concat(Object.keys(shapes).map(key => shapes[key]).map(shape => ({
        "title": shape.title,
        "shape": shape,
        "preset": false
    })));


const selectionsPropane = [
    {
        "title": `Cylindrical Dished 250 Gal.`,
        "preset": {

            "diameter": 31,
            "height": 31,
            "width": 31,

            "length": 72,

            "offset": 0,
            "outlet": 0,
            "safe": 100

        },
        shape: shapes["chd"]
    },
    {
        "title": `Cylindrical Dished 500 Gal.`,
        "preset": {

            "diameter": 37,
            "height": 37,
            "width": 37,

            "length": 102,

            "offset": 0,
            "outlet": 0,
            "safe": 100

        },
        shape: shapes["chd"]
    },
    {
        "title": `Cylindrical Dished 1000 Gal.`,
        "preset": {

            "diameter": 41,
            "height": 41,
            "width": 41,

            "length": 169,

            "offset": 0,
            "outlet": 0,
            "safe": 100
        },
        shape: shapes["chd"]
    },
    {
        "title": `Cylindrical Dished Other`,
        "preset": false,
        shape: shapes["chd"]
    },
    {
        "title": `Cylindrical Non-Dished`,
        "preset": false,
        shape: shapes["ch"]
    },

    {
        "title": `Cylindrical Vertical`,
        "preset": false,
        shape: shapes["cv"]
    }
];