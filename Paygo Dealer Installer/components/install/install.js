"use strict";


app.install = kendo.observable({

    type: 1,
    monitor: '',
    customer: '',
    container: '',
    ssid: '',
    password: '',
    manual: false,
    monitorFirst: false,

    currentRoute: app.config.override.route || "CUSTOMER",

    clear() {
        const install = app.install;

        // META
        install.setMonitorType(1);
        install.setMonitorFirstInstall(false);
        install.setIsManual(false);
        install.setCustomerTank("", "");
        install.setHotspot("", "");
        install.setMonitor("");

        // HOTSPOT
        app.selectHotspotView.set("ssid", "");
        app.selectHotspotView.set("password", "");

        // CUSTOMER
        app.selectCustomerView.set("filter", "");
        app.selectCustomerView.set("tank", "");
        app.selectCustomerView.set("hasTankList", false);
        app.selectCustomerView.set("hasError", false);
        app.selectCustomerView.set("tanks", []);

    },

    setMonitorType(type) {
        this.set("type", type);
    },

    setMonitorFirstInstall(monitorFirst) {
        this.set("monitorFirst", monitorFirst);
    },

    setIsManual(manual) {
        this.set("manual", manual);
    },
   
    setCustomerTank(customer, container) {
        this.set("customer", customer);
        this.set("container", container);
    },

    setHotspot(ssid, password) {
        this.set("ssid", ssid);
        this.set("password", password);
    },

    setMonitor(monitor) {
        app.install.set("monitor", monitor);
    },

    navigate() {
        app.install.route({
            preventDefault() {
            }
        });
    },

    go(route, back) {
        const install = app.install;

        let targetRoute;

        switch (route) {
            case "ACTIVATE-643":
                targetRoute = app.config.routes.activate643;
                break;
            case "ACTIVATED":
                targetRoute = app.config.routes.activated;
                break;
            case "CONFIGURE-608":
                targetRoute = app.config.routes.configure608;
                break;
            case "CONFIGURE-643":
                targetRoute = app.config.routes.configure643;
                break;
            case "MANUAL-643":
                targetRoute = app.config.routes.manual643;
                break;
            case "MANUAL-608":
                targetRoute = app.config.routes.manual608;
                break;
            case "PRIME-643":
                targetRoute = app.config.routes.prime643;
                break;
            case "PRIME-608":
                targetRoute = app.config.routes.prime608;
                break;
            case "PRIME-750":
                targetRoute = app.config.routes.prime750;
                break;
            case "CONFIRM":
                targetRoute = app.config.routes.confirm;
                break;
            case "CUSTOMER":
                targetRoute = app.config.routes.selectCustomer;
                break;
            case "SELECT":
                targetRoute = app.config.routes.selectMonitor;
                break;
            case "MONITOR":
                targetRoute = app.config.routes.monitorHotspot;
                break;
            case "GEOMETRY":
                targetRoute = app.config.routes.geometry;
                break;
            case "HOTSPOT":
                targetRoute = app.config.routes.selectHotspot;
                break;
            case "PRIME":
                targetRoute = app.config.routes.primeMonitor;
                break;
            default:
                targetRoute = app.config.routes.auth;
                break;
        }

        app.debug(`CURRENT:${install.currentRoute}`, `TARGET:${route}`);

        install.set("currentRoute", route);

        if (back) {
            app.mobileApp.navigate(targetRoute, "slide:right");
        } else {
            app.mobileApp.navigate(targetRoute, "slide");
        }
        
    },

    route(event) {
        event.preventDefault();

        const install = app.install;

        install.go(install.currentRoute);
    }

});

