"use strict";

app.primeMonitorView = kendo.observable({
    back() {
        if (app.install.monitorFirst) {
            app.install.go("SELECT", true);
        } else {
            app.install.go("HOTSPOT", true);
        }
    },

    continue() {
        if (app.install.monitorFirst || !window.WifiWizard || app.config.disableAutoDiscovery) {
            app.install.go("MONITOR");
            return;
        }

        app.loader(true);

        connectToMonitor()
            .then(getMonitorID)
            .then(monitor => {
                app.install.setMonitor(monitor.id);

                return setupMonitor(
                    app.comms.monitor.url,
                    app.comms.monitor.port,
                    app.install.ssid,
                    app.install.password
                );
            
                
            })
            .then(() => disconnect())
            .then(() => throttledPromise(() => app.comms.ping(), 7, 500, 500, true))
            .then(() => {
                app.loader(false);

                app.install.go("PRIME-750");
            })
            .catch(error => {
                app.loader(false);

                app.debug("PRIME-MONITOR", "FAILED TO AUTO CONECT TO MONITOR", error);
                app.install.go("MONITOR");
            });
    }

});

const disconnect = () => {
    const prime = app.primeMonitorView;
    app.debug("PRIME-MONITOR", "Disconnecting from PAYGO network");

    if (!prime.network) {
        app.debug("PRIME-MONITOR", "Not connected to network");

        return Promise.resolve();
    }

    app.debug("PRIME-MONITOR", prime.network);

    return new Promise(resolve => window.WifiWizard.disconnectNetwork(prime.network, resolve, resolve));

};

const getMonitorID = () => {
    app.debug("PRIME-MONITOR", "Getting monitor id");

    return new Promise((resolve, reject) => {
        $.ajax({
                method: 'GET',
                url: "http://192.168.4.1",
                timeout: 1000
            })
            .done(response => {
                const document = $(response);
                const id = +document.find(".style23").text().trim();

                app.debug("PRIME-MONITOR", "Obtained monitor id", id);

                if (!id) {
                    reject("Faulty monitor id found on GREMLIN® configuration page");
                    return;
                }

                const ssids = [];

                const options = document.find("option");
                options.each(function() {
                    const ssid = $(this).val().trim();
                    if (ssid === "choose_from_list" || ssid === "manually_entered") {
                        return;
                    }

                    ssids.push({
                        SSID: ssid
                    });
                });

                resolve({
                    id: id,
                    ssids: ssids
                });
            })
            .fail(reject);
    });
}

const connectToMonitor = () => new Promise((finish, fatal) => {

    let counter = 0;

    const errorHandler = (error) => {
        if (error === -1) {

            if (counter > 4) {
                fatal("Could not find monitor hotspot");
                return;
            }

            counter++;

            app.debug("PRIME-MONITOR", "Could not find monitor hotspot, re-attempting after 2 seconds");

            setTimeout(connect, 2000);

            return;
        }

        fatal(error);
    };

    function discover() {
        app.debug("PRIME-MONITOR", "Starting WiFi scan");

        return new Promise((resolve, reject) => {
            window.WifiWizard.startScan(
                () => onNetworkDiscovery(resolve, reject),
                reject
            );
        });
    };

    function connect() {
        return discover().then(finish).catch(errorHandler);
    }

    connect();
});

const onNetworkDiscovery = (resolve, reject) => {
    app.debug("PRIME-MONITOR", "Scan start OK, attaching callback for results");

    window.WifiWizard.getScanResults(
        networks => findAndConnectToMonitor(networks, resolve, reject),
        error => reject(error)
    );
};

const findAndConnectToMonitor = (networks, resolve, reject) => {
    app.debug("PRIME-MONITOR", "Scan results OK, finding monitor and connecting", networks);

    const paygoNetwork = networks.find(network => {
        const ssid = (network.SSID || "");

        return ssid.startsWith("PAYGO") || ssid.startsWith("GREMLIN");
    });

    if (!paygoNetwork) {
        app.debug("PRIME-MONITOR", "Could not find hotspot");

        reject(-1);
        return;
    }

    app.primeMonitorView.set("network", paygoNetwork.SSID);
    
    connectToWIFINetwork(paygoNetwork.SSID, "")
        .then(() => {
            app.debug("PRIME-MONITOR", "Connected to hotspot.. pinging monitor");
            //ATTEMPT 3 attempts of 3 seconds each to connect
            return throttledPromise(() => app.comms.get("http://192.168.4.1"), 5, 2500)
                .then(resolve)
                .catch(reject);
        })
        .catch(reject);
};

function throttledPromise(promise, reAttempts, timeout, delay, shouldAlwaysSucceed) {
    if (arguments.length === 3) {
        delay = timeout;
    }

    let count = 0;

    return new Promise((finish, fatal) => {

        const errorHandler = (error) => {
            if (count > reAttempts) {
                if (shouldAlwaysSucceed) {
                    finish();
                    return;
                }

                fatal(error);
                return;
            }

            count++;

            app.debug("THROTTLED-PROMISE", "Error performing task", error);
            app.debug("THROTTLED-PROMISE", `Re-executing in ${timeout}ms`, reAttempts, count, shouldAlwaysSucceed);

            setTimeout(execute, 2000);
        };

        function execute() {
            return promise()
                .then(finish)
                .catch(errorHandler);
        }

        if (delay) {
            setTimeout(execute, delay);
        } else {
            execute();
        }

    });
}