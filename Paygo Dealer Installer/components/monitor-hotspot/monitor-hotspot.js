"use strict";

app.monitorHotspotView = kendo.observable({

    ssids: [],

    back() {
        app.install.go("PRIME", true);
    },

    continue() {
        app.loader(true);


        getMonitorID()
            .then((monitor) => {
                app.install.setMonitor(monitor.id);

                if (app.install.monitorFirst) {

                    app.debug(
                        "MONITOR-HOTSPOT",
                        "Getting hotspot list >",
                        monitor.ssids
                    );

                    app.monitorHotspotView.set("ssids", monitor.ssids);

                    app.install.go("HOTSPOT");

                    app.loader(false);

                    return Promise.resolve(true);
                } else {
                    return setupMonitor(
                            app.comms.monitor.url,
                            app.comms.monitor.port,
                            app.install.ssid,
                            app.install.password
                        ).then(() => disconnect())
                    .then(() => throttledPromise(() => app.comms.ping(), 7, 500, 500, true))
                    .then(() => {
                        app.loader(false);

                        app.install.go("PRIME-750");
                    });
                }
            })
            .catch(reason => {
                app.loader(false);


                app.debug(
                    "MONITOR-HOTSPOT",
                    "Failed to connect to monitor >",
                    reason
                );


                if (app.config.override && app.config.override.ignoreMonitorConnect) {
                    app.install.go("PRIME-750");

                    app.debug(
                        "MONITOR-HOTSPOT",
                        "Overriding errorflow and proceeding to the next monitor step"
                    );
                    return;
                }

                app.message("GREMLIN® Monitor",
                    `<p>We could not connect to the monitor.<p><p>Please make sure you are connected to the WiFi network that starts with <b>PAYGO</b>.<p>`
                );
            });
    }

});

const setupMonitor = (url, port, ssid, password) => {
    app.debug("MONITOR-HOTSPOT", "Sending WiFi information to monitor");

    app.debug("MONITOR-HOTSPOT", "SSID > ", ssid);
    app.debug("MONITOR-HOTSPOT", "PASSOWRD > ", password);
    app.debug("MONITOR-HOTSPOT", "TMS URL > ", url);
    app.debug("MONITOR-HOTSPOT", "TMS PORT > ", port);

    return new Promise((resolve, reject) => {

        const xhr = new XMLHttpRequest(); //what the fu**. 
                                          //need to revisit when I have a minute

        xhr.open("POST", "http://192.168.4.1/data?command=send", true);
        xhr.setRequestHeader('Content-type', 'text/plain');

        let message = "TEKSettings={'SURL':'";
        message += url; 
        message += "','SPORT':'";
        message += port;
        message += "'}";

        xhr.timeout = 2500;
        xhr.send(message);

        const payload = {
            ssid: ssid,
            password: password
        };

        xhr.onloadend = function () {
            if (xhr.status !== 200) {
                reject("Failed to send monitor setting");
                return;
            }

            $.ajax({
                method: 'POST',
                url: "http://192.168.4.1/config?command=wifi",
                headers: { 'Content-type': 'text/plain' },
                data: JSON.stringify(payload)
            })
            .done(resolve)
            .fail(reject);
        };
    });
}
