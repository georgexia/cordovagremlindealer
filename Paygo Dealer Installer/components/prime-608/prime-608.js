"use strict";

app.prime608View = kendo.observable({

    timeout: false,

    init() {

        $.ajax({
            url: app.config.firmwareEndpoint,
            type: "POST",
            dataType: "text",
            data: JSON.stringify({
                serial: app.install.monitor,
                new: true
            })
        });

        if (app.prime608View.timeout) {
            return;
        }

        app.prime608View.set("timeout",
            setTimeout(
                () => {
                    if (app.install.currentRoute === "PRIME-608") {
                        app.install.go("GEOMETRY");
                    }

                    app.prime608View.set("timeout", false);
                },
                2500
            )
        );

    }

});
