"use strict";

app.prime750View = kendo.observable({

    toggled:false,

    back() {
        if (app.install.monitorFirst || !window.WifiWizard || app.config.disableAutoDiscovery) {
            app.install.go("MONITOR", true);
        } else {
            app.install.go("PRIME", true);
        }
    },

    continue() {
        app.install.go("GEOMETRY");
    },

    connect() {
        if (app.install.monitorFirst || !window.WifiWizardy || app.config.disableAutoDiscovery) {
            app.install.go("PRIME", true);
        } else {
            app.install.go("HOTSPOT", true);
        }
    },

    toggle() {
        const prime = app.prime750View;

        prime.set("toggled", !prime.toggled);
    }

});
