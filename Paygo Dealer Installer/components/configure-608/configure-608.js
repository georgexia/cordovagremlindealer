"use strict";

app.configure608View = kendo.observable({

    back() {
        app.install.go("SELECT", true);
    },

    manual() {
        app.debug("CONFIGURE-608", "Proceeding to manual insert");

        app.install.setIsManual(true);

        app.install.go("MANUAL-608");
    },

    scan() {
        app.debug("CONFIGURE-608", "Proceeding to barcode scan");

        if (typeof cordova === 'undefined' ||
            typeof cordova.plugins === 'undefined' ||
            typeof cordova.plugins.barcodeScanner === 'undefined') {
            app.configure608View.manual();
            return;
        }

        cordova.plugins.barcodeScanner.scan(

            // success callback function
            function (result) {
                if (result.cancelled) {
                    return;
                }

                app.install.setIsManual(false);
                app.install.setMonitor(result.text);
                app.install.go("CONFIRM");

            },

            // error callback function
            function (error) {
                alert("Scanning failed: " + error);
            },

            // options object
            {
                "preferFrontCamera": false,
                "showFlipCameraButton": true,
                "showTorchButton": true,
                "orientation": "landscape"
            }
        )

    }

});
