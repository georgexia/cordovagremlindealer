"use strict";

app.activatedView = kendo.observable({

    serial: "",
    continueMessage: "",
    retryMessage: "",
    canRetry: false,
    canContinue: false,

    back() {
        const confirm = app.activatedView;

        confirm.set("confirming", false);

        app.install.go("ACTIVATE-643", true);
    },

    continue() {
        app.install.go("GEOMETRY");
    },

    retry() {

        app.install.go("PRIME-643");
    }

});