"use strict";

app.manual608View = kendo.observable({

    error: false,
    serial: "",

    back() {
        app.install.go("CONFIGURE-608", true);
    },

    continue() {
        const configuration = app.manual608View;

        app.debug("MANUAL-608", "Proceeding with serial > ", configuration.serial);

        if (!configuration.serial) {
            configuration.set("error", true);
            return;
        }

        configuration.set("error", false);

        app.install.setMonitor(configuration.serial);
        app.install.go("CONFIRM");

    }

});
