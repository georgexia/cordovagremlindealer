"use strict";

(function(comms, config) {

    comms.routes = {};
    comms.monitor = {};

    comms.routes.config = "/tms/config";
    comms.routes.auth = "/signin"; 
    comms.routes.ping = "/ping"; 
    comms.routes.customerAccounts = (subscriber, dealer, filter) =>
            `/api/subscribers/${subscriber}/customerAccounts` +
            `?pageNumber=1` +
            `&sortBy=name` +
            `&ascending=true` +
            `&pageSize=25` +
            `&searchTerm=${filter}` +
        `&dealerId=${dealer}`;
    comms.routes.container = (monitor) =>
        `/api/monitors/${monitor}/container`;
    comms.routes.geometry = (subscriber, tank) =>
        `/api/subscribers/${subscriber}/containers/${tank}/monitors`;
    comms.routes.logo = (subscriber) => `/api/subscribers/${subscriber}/logo`;

    comms.geometry = function (tank, location, antenna, shape, height, width, length, offset, outlet, capacity, type) {
        return comms.put(comms.getEndpoint(
            "geometry",
            comms.tokens.subscriber,
            tank
        ),
        {
            locationId: location,
            monitorAntennaTypeId: antenna,
            shapeId: shape,
            height: height,
            width: width,
            length: length,
            sensorOffset: offset,
            outletHeight: outlet,
            safeCapacity: capacity,
            type: type
        });
    };


    comms.map = function (tank, monitor) {
        return comms.post(comms.getEndpoint(
            "geometry",
            comms.tokens.subscriber,
            tank
        ),
        { 
            serialNumber: monitor,
            type: "TMS"
        });
    };
 
    comms.container = function (monitor) {
        return comms.get(comms.getEndpoint(
            "container",
            monitor
        ));
    };

    comms.ping = function (timeout) {
        if (!timeout) {
            timeout = 1000;
        };

        return new Promise((resolve, reject) => {
            app.debug("COMMS-PING");

            $.ajax({
                url: comms.getEndpoint("ping"),
                type: "GET",
                timeout: timeout
            }).done(resolve).fail(reject);
        });
        
    };

    comms.tanks = function(filter) {
        return comms.get(comms.getEndpoint(
            "customerAccounts",
            comms.tokens.subscriber,
            comms.tokens.dealer,
            filter
        )).then(results => {
            var tanks = [];

            results.forEach(customer => {
                customer.containers.forEach(container => {
                    container.customerId = customer.customerId;
                    container.customer = customer.name;

                    tanks.push(container);
                });
            });

            return tanks;
        });
    };

    comms.auth = function(username, password) {
        return comms.post(comms.getEndpoint("auth"),
        {
            username: username,
            password: password
        }).then(function(response) {
            comms.tokens.access = response.token;
            comms.tokens.dealer = response.user.hierarchy.dealer.nodeId;
            comms.tokens.subscriber = response.user.subscriberId;

            comms.get(
                comms.getEndpoint(
                    "logo",
                    comms.tokens.subscriber
                )
            ).then(logo => {
                app.debug("COMMS-AUTH", `Logo for subscriber found: ${logo}`);

                //$("#logo").attr("src", logo); 

                app.state.set("logo", logo);
                app.state.set("branding", true);
            });

            app.state.set("auth", true);

            app.debug("COMMS-AUTH", comms.tokens);
        });
    };

    comms.setABOSEndpoint = function(target, dealer) {
        if (config.debug && config.override && config.override.target) {
            target = config.override.target;
        }
        comms.target = target;

        let url = config.endpoints[target] + comms.routes.config;

        return comms.get(url,
        {
            code: dealer
        }).then(function(config) {
            let endpoint = config.tmsAPI.url;

            if (config.tmsAPI.port
                && config.tmsAPI.port > 0) {

                endpoint += `:${config.tmsAPI.port}`;
            }

            comms.endpoint = endpoint;

            console.log(comms.endpoint);

            comms.monitor.url = config.monitorAPI.url;
            comms.monitor.port = config.monitorAPI.port;

            app.debug("COMMS-ENDPOINT", endpoint, comms.monitor);
        });
    };

    comms.getEndpoint = function(route) {
        let base = comms.endpoint;

        //if (config.target && config.target !== comms.target) {
        //    base = config.endpoints[comms.target];
        //}
        if (config.debug && config.override && config.override.target) {
            base = config.endpoints[config.override.target];
        }

        let resolvedRoute = `/${route}`;

        if (comms.routes.hasOwnProperty(route)) {
            resolvedRoute = comms.routes[route];
            if (typeof resolvedRoute !== 'string') {
                let parameters = Array.from(arguments).slice(1);

                resolvedRoute = resolvedRoute.apply(comms, parameters);
            }
        }

        return base + resolvedRoute;
    };

    comms.put = function (url, data, noAuth) {
        return comms.request("put", url, data, noAuth);
    };

    comms.post = function (url, data, noAuth) {
        return comms.request("post", url, data, noAuth);
    };

    comms.get = function(url, data, noAuth) {
        return comms.request("get", url, data, noAuth);
    };

    comms.request = function (method, url, data, noAuth) {
        data = data || {};
        method = method.toUpperCase();
        
        let originalData = data;
        let headers = {};

        if (!noAuth) {
            if (config.debug && config.override && config.override.tokens && config.override.tokens.access) {
                headers["x-access-token"] = config.override.tokens.access;
            } else if (comms.tokens && comms.tokens.access) {
                headers["x-access-token"] = comms.tokens.access;
            }
        }
        

        app.debug(`COMMS-${method}`, url, data, headers);
        //debugger;
        return new Promise(function(resolve, reject) {
            $.ajax({
                    url: url,
                    type: method,
                    headers: headers,
                    data: data
                })
                .done(function(data) {
                    app.debug(`COMMS-${method}-RESPONSE`, url, data);

                    resolve(data);
                })
                .fail(function (data) {
                    if (comms.target !== "prod") {
                        reject(JSON.stringify({
                            "target": comms.target,
                            "endpoint": url,
                            "data": originalData,
                            "headers": headers,
                            "response": data
                        }, null, 4));
                        return;
                    }

                    app.debug(`COMMS-${method}-FAIL`, url, data);

                    let message = data.responseText;
                    if (!message) {
                        message = "There was an error completing the request. Please try again.";
                    } else {
                        try {
                            let jsonMessage = JSON.parse(message);
                            if (jsonMessage.msg || jsonMessage.message) {
                                // Brite ERROR
                                message = jsonMessage.msg || jsonMessage.message;
                            }
                        } catch (err) {

                        }
                    }

                    reject(message);
                });
        });
    };

    comms.reset = function() {
        comms.endpoint = null;
        comms.tokens = {};
    };

})(window.app.comms = {}, window.app.config);